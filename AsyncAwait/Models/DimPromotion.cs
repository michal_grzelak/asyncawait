﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimPromotion
    {
        public DimPromotion()
        {
            FactInternetSales = new HashSet<FactInternetSales>();
            FactResellerSales = new HashSet<FactResellerSales>();
        }

        public int PromotionKey { get; set; }
        public int? PromotionAlternateKey { get; set; }
        [StringLength(255)]
        public string EnglishPromotionName { get; set; }
        [StringLength(255)]
        public string SpanishPromotionName { get; set; }
        [StringLength(255)]
        public string FrenchPromotionName { get; set; }
        public double? DiscountPct { get; set; }
        [StringLength(50)]
        public string EnglishPromotionType { get; set; }
        [StringLength(50)]
        public string SpanishPromotionType { get; set; }
        [StringLength(50)]
        public string FrenchPromotionType { get; set; }
        [StringLength(50)]
        public string EnglishPromotionCategory { get; set; }
        [StringLength(50)]
        public string SpanishPromotionCategory { get; set; }
        [StringLength(50)]
        public string FrenchPromotionCategory { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime StartDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndDate { get; set; }
        public int? MinQty { get; set; }
        public int? MaxQty { get; set; }

        [InverseProperty("PromotionKeyNavigation")]
        public virtual ICollection<FactInternetSales> FactInternetSales { get; set; }
        [InverseProperty("PromotionKeyNavigation")]
        public virtual ICollection<FactResellerSales> FactResellerSales { get; set; }
    }
}
