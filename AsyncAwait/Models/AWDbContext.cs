﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AsyncAwait.Models
{
    public partial class AWDbContext : DbContext
    {
        public AWDbContext()
        {
        }

        public AWDbContext(DbContextOptions<AWDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DatabaseLog> DatabaseLog { get; set; }
        public virtual DbSet<DimAccount> DimAccount { get; set; }
        public virtual DbSet<DimCurrency> DimCurrency { get; set; }
        public virtual DbSet<DimCustomer> DimCustomer { get; set; }
        public virtual DbSet<DimDate> DimDate { get; set; }
        public virtual DbSet<DimDepartmentGroup> DimDepartmentGroup { get; set; }
        public virtual DbSet<DimEmployee> DimEmployee { get; set; }
        public virtual DbSet<DimGeography> DimGeography { get; set; }
        public virtual DbSet<DimOrganization> DimOrganization { get; set; }
        public virtual DbSet<DimProduct> DimProduct { get; set; }
        public virtual DbSet<DimProductCategory> DimProductCategory { get; set; }
        public virtual DbSet<DimProductSubcategory> DimProductSubcategory { get; set; }
        public virtual DbSet<DimPromotion> DimPromotion { get; set; }
        public virtual DbSet<DimReseller> DimReseller { get; set; }
        public virtual DbSet<DimSalesReason> DimSalesReason { get; set; }
        public virtual DbSet<DimSalesTerritory> DimSalesTerritory { get; set; }
        public virtual DbSet<DimScenario> DimScenario { get; set; }
        public virtual DbSet<FactAdditionalInternationalProductDescription> FactAdditionalInternationalProductDescription { get; set; }
        public virtual DbSet<FactCallCenter> FactCallCenter { get; set; }
        public virtual DbSet<FactCurrencyRate> FactCurrencyRate { get; set; }
        public virtual DbSet<FactInternetSales> FactInternetSales { get; set; }
        public virtual DbSet<FactInternetSalesReason> FactInternetSalesReason { get; set; }
        public virtual DbSet<FactProductInventory> FactProductInventory { get; set; }
        public virtual DbSet<FactResellerSales> FactResellerSales { get; set; }
        public virtual DbSet<FactSalesQuota> FactSalesQuota { get; set; }
        public virtual DbSet<FactSurveyResponse> FactSurveyResponse { get; set; }
        public virtual DbSet<ProspectiveBuyer> ProspectiveBuyer { get; set; }

        // Unable to generate entity type for table 'dbo.AdventureWorksDWBuildVersion'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FactFinance'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.NewFactCurrencyRate'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=AdventureWorksDW;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<DatabaseLog>(entity =>
            {
                entity.HasKey(e => e.DatabaseLogId)
                    .HasName("PK_DatabaseLog_DatabaseLogID")
                    .ForSqlServerIsClustered(false);
            });

            modelBuilder.Entity<DimAccount>(entity =>
            {
                entity.HasOne(d => d.ParentAccountKeyNavigation)
                    .WithMany(p => p.InverseParentAccountKeyNavigation)
                    .HasForeignKey(d => d.ParentAccountKey)
                    .HasConstraintName("FK_DimAccount_DimAccount");
            });

            modelBuilder.Entity<DimCurrency>(entity =>
            {
                entity.HasKey(e => e.CurrencyKey)
                    .HasName("PK_DimCurrency_CurrencyKey");

                entity.HasIndex(e => e.CurrencyAlternateKey)
                    .HasName("AK_DimCurrency_CurrencyAlternateKey")
                    .IsUnique();
            });

            modelBuilder.Entity<DimCustomer>(entity =>
            {
                entity.HasKey(e => e.CustomerKey)
                    .HasName("PK_DimCustomer_CustomerKey");

                entity.HasIndex(e => e.CustomerAlternateKey)
                    .IsUnique();

                entity.HasOne(d => d.GeographyKeyNavigation)
                    .WithMany(p => p.DimCustomer)
                    .HasForeignKey(d => d.GeographyKey)
                    .HasConstraintName("FK_DimCustomer_DimGeography");
            });

            modelBuilder.Entity<DimDate>(entity =>
            {
                entity.HasKey(e => e.DateKey)
                    .HasName("PK_DimDate_DateKey");

                entity.HasIndex(e => e.FullDateAlternateKey)
                    .HasName("AK_DimDate_FullDateAlternateKey")
                    .IsUnique();

                entity.Property(e => e.DateKey).ValueGeneratedNever();
            });

            modelBuilder.Entity<DimDepartmentGroup>(entity =>
            {
                entity.HasOne(d => d.ParentDepartmentGroupKeyNavigation)
                    .WithMany(p => p.InverseParentDepartmentGroupKeyNavigation)
                    .HasForeignKey(d => d.ParentDepartmentGroupKey)
                    .HasConstraintName("FK_DimDepartmentGroup_DimDepartmentGroup");
            });

            modelBuilder.Entity<DimEmployee>(entity =>
            {
                entity.HasKey(e => e.EmployeeKey)
                    .HasName("PK_DimEmployee_EmployeeKey");

                entity.HasOne(d => d.ParentEmployeeKeyNavigation)
                    .WithMany(p => p.InverseParentEmployeeKeyNavigation)
                    .HasForeignKey(d => d.ParentEmployeeKey)
                    .HasConstraintName("FK_DimEmployee_DimEmployee");

                entity.HasOne(d => d.SalesTerritoryKeyNavigation)
                    .WithMany(p => p.DimEmployee)
                    .HasForeignKey(d => d.SalesTerritoryKey)
                    .HasConstraintName("FK_DimEmployee_DimSalesTerritory");
            });

            modelBuilder.Entity<DimGeography>(entity =>
            {
                entity.HasKey(e => e.GeographyKey)
                    .HasName("PK_DimGeography_GeographyKey");

                entity.HasOne(d => d.SalesTerritoryKeyNavigation)
                    .WithMany(p => p.DimGeography)
                    .HasForeignKey(d => d.SalesTerritoryKey)
                    .HasConstraintName("FK_DimGeography_DimSalesTerritory");
            });

            modelBuilder.Entity<DimOrganization>(entity =>
            {
                entity.HasOne(d => d.CurrencyKeyNavigation)
                    .WithMany(p => p.DimOrganization)
                    .HasForeignKey(d => d.CurrencyKey)
                    .HasConstraintName("FK_DimOrganization_DimCurrency");

                entity.HasOne(d => d.ParentOrganizationKeyNavigation)
                    .WithMany(p => p.InverseParentOrganizationKeyNavigation)
                    .HasForeignKey(d => d.ParentOrganizationKey)
                    .HasConstraintName("FK_DimOrganization_DimOrganization");
            });

            modelBuilder.Entity<DimProduct>(entity =>
            {
                entity.HasKey(e => e.ProductKey)
                    .HasName("PK_DimProduct_ProductKey");

                entity.HasIndex(e => new { e.ProductAlternateKey, e.StartDate })
                    .HasName("AK_DimProduct_ProductAlternateKey_StartDate")
                    .IsUnique();

                entity.HasOne(d => d.ProductSubcategoryKeyNavigation)
                    .WithMany(p => p.DimProduct)
                    .HasForeignKey(d => d.ProductSubcategoryKey)
                    .HasConstraintName("FK_DimProduct_DimProductSubcategory");
            });

            modelBuilder.Entity<DimProductCategory>(entity =>
            {
                entity.HasKey(e => e.ProductCategoryKey)
                    .HasName("PK_DimProductCategory_ProductCategoryKey");

                entity.HasIndex(e => e.ProductCategoryAlternateKey)
                    .HasName("AK_DimProductCategory_ProductCategoryAlternateKey")
                    .IsUnique();
            });

            modelBuilder.Entity<DimProductSubcategory>(entity =>
            {
                entity.HasKey(e => e.ProductSubcategoryKey)
                    .HasName("PK_DimProductSubcategory_ProductSubcategoryKey");

                entity.HasIndex(e => e.ProductSubcategoryAlternateKey)
                    .HasName("AK_DimProductSubcategory_ProductSubcategoryAlternateKey")
                    .IsUnique();

                entity.HasOne(d => d.ProductCategoryKeyNavigation)
                    .WithMany(p => p.DimProductSubcategory)
                    .HasForeignKey(d => d.ProductCategoryKey)
                    .HasConstraintName("FK_DimProductSubcategory_DimProductCategory");
            });

            modelBuilder.Entity<DimPromotion>(entity =>
            {
                entity.HasKey(e => e.PromotionKey)
                    .HasName("PK_DimPromotion_PromotionKey");

                entity.HasIndex(e => e.PromotionAlternateKey)
                    .HasName("AK_DimPromotion_PromotionAlternateKey")
                    .IsUnique();
            });

            modelBuilder.Entity<DimReseller>(entity =>
            {
                entity.HasKey(e => e.ResellerKey)
                    .HasName("PK_DimReseller_ResellerKey");

                entity.HasIndex(e => e.ResellerAlternateKey)
                    .HasName("AK_DimReseller_ResellerAlternateKey")
                    .IsUnique();

                entity.Property(e => e.BusinessType).IsUnicode(false);

                entity.Property(e => e.OrderFrequency).IsUnicode(false);

                entity.HasOne(d => d.GeographyKeyNavigation)
                    .WithMany(p => p.DimReseller)
                    .HasForeignKey(d => d.GeographyKey)
                    .HasConstraintName("FK_DimReseller_DimGeography");
            });

            modelBuilder.Entity<DimSalesReason>(entity =>
            {
                entity.HasKey(e => e.SalesReasonKey)
                    .HasName("PK_DimSalesReason_SalesReasonKey");
            });

            modelBuilder.Entity<DimSalesTerritory>(entity =>
            {
                entity.HasKey(e => e.SalesTerritoryKey)
                    .HasName("PK_DimSalesTerritory_SalesTerritoryKey");

                entity.HasIndex(e => e.SalesTerritoryAlternateKey)
                    .HasName("AK_DimSalesTerritory_SalesTerritoryAlternateKey")
                    .IsUnique();
            });

            modelBuilder.Entity<FactAdditionalInternationalProductDescription>(entity =>
            {
                entity.HasKey(e => new { e.ProductKey, e.CultureName })
                    .HasName("PK_FactAdditionalInternationalProductDescription_ProductKey_CultureName");
            });

            modelBuilder.Entity<FactCallCenter>(entity =>
            {
                entity.HasIndex(e => new { e.DateKey, e.Shift })
                    .HasName("AK_FactCallCenter_DateKey_Shift")
                    .IsUnique();

                entity.HasOne(d => d.DateKeyNavigation)
                    .WithMany(p => p.FactCallCenter)
                    .HasForeignKey(d => d.DateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactCallCenter_DimDate");
            });

            modelBuilder.Entity<FactCurrencyRate>(entity =>
            {
                entity.HasKey(e => new { e.CurrencyKey, e.DateKey })
                    .HasName("PK_FactCurrencyRate_CurrencyKey_DateKey");

                entity.HasOne(d => d.CurrencyKeyNavigation)
                    .WithMany(p => p.FactCurrencyRate)
                    .HasForeignKey(d => d.CurrencyKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactCurrencyRate_DimCurrency");

                entity.HasOne(d => d.DateKeyNavigation)
                    .WithMany(p => p.FactCurrencyRate)
                    .HasForeignKey(d => d.DateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactCurrencyRate_DimDate");
            });

            modelBuilder.Entity<FactInternetSales>(entity =>
            {
                entity.HasKey(e => new { e.SalesOrderNumber, e.SalesOrderLineNumber })
                    .HasName("PK_FactInternetSales_SalesOrderNumber_SalesOrderLineNumber");

                entity.HasOne(d => d.CurrencyKeyNavigation)
                    .WithMany(p => p.FactInternetSales)
                    .HasForeignKey(d => d.CurrencyKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactInternetSales_DimCurrency");

                entity.HasOne(d => d.CustomerKeyNavigation)
                    .WithMany(p => p.FactInternetSales)
                    .HasForeignKey(d => d.CustomerKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactInternetSales_DimCustomer");

                entity.HasOne(d => d.DueDateKeyNavigation)
                    .WithMany(p => p.FactInternetSalesDueDateKeyNavigation)
                    .HasForeignKey(d => d.DueDateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactInternetSales_DimDate1");

                entity.HasOne(d => d.OrderDateKeyNavigation)
                    .WithMany(p => p.FactInternetSalesOrderDateKeyNavigation)
                    .HasForeignKey(d => d.OrderDateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactInternetSales_DimDate");

                entity.HasOne(d => d.ProductKeyNavigation)
                    .WithMany(p => p.FactInternetSales)
                    .HasForeignKey(d => d.ProductKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactInternetSales_DimProduct");

                entity.HasOne(d => d.PromotionKeyNavigation)
                    .WithMany(p => p.FactInternetSales)
                    .HasForeignKey(d => d.PromotionKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactInternetSales_DimPromotion");

                entity.HasOne(d => d.SalesTerritoryKeyNavigation)
                    .WithMany(p => p.FactInternetSales)
                    .HasForeignKey(d => d.SalesTerritoryKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactInternetSales_DimSalesTerritory");

                entity.HasOne(d => d.ShipDateKeyNavigation)
                    .WithMany(p => p.FactInternetSalesShipDateKeyNavigation)
                    .HasForeignKey(d => d.ShipDateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactInternetSales_DimDate2");
            });

            modelBuilder.Entity<FactInternetSalesReason>(entity =>
            {
                entity.HasKey(e => new { e.SalesOrderNumber, e.SalesOrderLineNumber, e.SalesReasonKey })
                    .HasName("PK_FactInternetSalesReason_SalesOrderNumber_SalesOrderLineNumber_SalesReasonKey");

                entity.HasOne(d => d.SalesReasonKeyNavigation)
                    .WithMany(p => p.FactInternetSalesReason)
                    .HasForeignKey(d => d.SalesReasonKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactInternetSalesReason_DimSalesReason");

                entity.HasOne(d => d.SalesOrder)
                    .WithMany(p => p.FactInternetSalesReason)
                    .HasForeignKey(d => new { d.SalesOrderNumber, d.SalesOrderLineNumber })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactInternetSalesReason_FactInternetSales");
            });

            modelBuilder.Entity<FactProductInventory>(entity =>
            {
                entity.HasKey(e => new { e.ProductKey, e.DateKey });

                entity.HasOne(d => d.DateKeyNavigation)
                    .WithMany(p => p.FactProductInventory)
                    .HasForeignKey(d => d.DateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactProductInventory_DimDate");

                entity.HasOne(d => d.ProductKeyNavigation)
                    .WithMany(p => p.FactProductInventory)
                    .HasForeignKey(d => d.ProductKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactProductInventory_DimProduct");
            });

            modelBuilder.Entity<FactResellerSales>(entity =>
            {
                entity.HasKey(e => new { e.SalesOrderNumber, e.SalesOrderLineNumber })
                    .HasName("PK_FactResellerSales_SalesOrderNumber_SalesOrderLineNumber");

                entity.HasOne(d => d.CurrencyKeyNavigation)
                    .WithMany(p => p.FactResellerSales)
                    .HasForeignKey(d => d.CurrencyKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactResellerSales_DimCurrency");

                entity.HasOne(d => d.DueDateKeyNavigation)
                    .WithMany(p => p.FactResellerSalesDueDateKeyNavigation)
                    .HasForeignKey(d => d.DueDateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactResellerSales_DimDate1");

                entity.HasOne(d => d.EmployeeKeyNavigation)
                    .WithMany(p => p.FactResellerSales)
                    .HasForeignKey(d => d.EmployeeKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactResellerSales_DimEmployee");

                entity.HasOne(d => d.OrderDateKeyNavigation)
                    .WithMany(p => p.FactResellerSalesOrderDateKeyNavigation)
                    .HasForeignKey(d => d.OrderDateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactResellerSales_DimDate");

                entity.HasOne(d => d.ProductKeyNavigation)
                    .WithMany(p => p.FactResellerSales)
                    .HasForeignKey(d => d.ProductKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactResellerSales_DimProduct");

                entity.HasOne(d => d.PromotionKeyNavigation)
                    .WithMany(p => p.FactResellerSales)
                    .HasForeignKey(d => d.PromotionKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactResellerSales_DimPromotion");

                entity.HasOne(d => d.ResellerKeyNavigation)
                    .WithMany(p => p.FactResellerSales)
                    .HasForeignKey(d => d.ResellerKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactResellerSales_DimReseller");

                entity.HasOne(d => d.SalesTerritoryKeyNavigation)
                    .WithMany(p => p.FactResellerSales)
                    .HasForeignKey(d => d.SalesTerritoryKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactResellerSales_DimSalesTerritory");

                entity.HasOne(d => d.ShipDateKeyNavigation)
                    .WithMany(p => p.FactResellerSalesShipDateKeyNavigation)
                    .HasForeignKey(d => d.ShipDateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactResellerSales_DimDate2");
            });

            modelBuilder.Entity<FactSalesQuota>(entity =>
            {
                entity.HasKey(e => e.SalesQuotaKey)
                    .HasName("PK_FactSalesQuota_SalesQuotaKey");

                entity.HasOne(d => d.DateKeyNavigation)
                    .WithMany(p => p.FactSalesQuota)
                    .HasForeignKey(d => d.DateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactSalesQuota_DimDate");

                entity.HasOne(d => d.EmployeeKeyNavigation)
                    .WithMany(p => p.FactSalesQuota)
                    .HasForeignKey(d => d.EmployeeKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactSalesQuota_DimEmployee");
            });

            modelBuilder.Entity<FactSurveyResponse>(entity =>
            {
                entity.HasKey(e => e.SurveyResponseKey)
                    .HasName("PK_FactSurveyResponse_SurveyResponseKey");

                entity.HasOne(d => d.CustomerKeyNavigation)
                    .WithMany(p => p.FactSurveyResponse)
                    .HasForeignKey(d => d.CustomerKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactSurveyResponse_CustomerKey");

                entity.HasOne(d => d.DateKeyNavigation)
                    .WithMany(p => p.FactSurveyResponse)
                    .HasForeignKey(d => d.DateKey)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FactSurveyResponse_DateKey");
            });

            modelBuilder.Entity<ProspectiveBuyer>(entity =>
            {
                entity.HasKey(e => e.ProspectiveBuyerKey)
                    .HasName("PK_ProspectiveBuyer_ProspectiveBuyerKey");
            });
        }
    }
}
