﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimAccount
    {
        public DimAccount()
        {
            InverseParentAccountKeyNavigation = new HashSet<DimAccount>();
        }

        [Key]
        public int AccountKey { get; set; }
        public int? ParentAccountKey { get; set; }
        public int? AccountCodeAlternateKey { get; set; }
        public int? ParentAccountCodeAlternateKey { get; set; }
        [StringLength(50)]
        public string AccountDescription { get; set; }
        [StringLength(50)]
        public string AccountType { get; set; }
        [StringLength(50)]
        public string Operator { get; set; }
        [StringLength(300)]
        public string CustomMembers { get; set; }
        [StringLength(50)]
        public string ValueType { get; set; }
        [StringLength(200)]
        public string CustomMemberOptions { get; set; }

        [ForeignKey("ParentAccountKey")]
        [InverseProperty("InverseParentAccountKeyNavigation")]
        public virtual DimAccount ParentAccountKeyNavigation { get; set; }
        [InverseProperty("ParentAccountKeyNavigation")]
        public virtual ICollection<DimAccount> InverseParentAccountKeyNavigation { get; set; }
    }
}
