﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimProductSubcategory
    {
        public DimProductSubcategory()
        {
            DimProduct = new HashSet<DimProduct>();
        }

        public int ProductSubcategoryKey { get; set; }
        public int? ProductSubcategoryAlternateKey { get; set; }
        [Required]
        [StringLength(50)]
        public string EnglishProductSubcategoryName { get; set; }
        [Required]
        [StringLength(50)]
        public string SpanishProductSubcategoryName { get; set; }
        [Required]
        [StringLength(50)]
        public string FrenchProductSubcategoryName { get; set; }
        public int? ProductCategoryKey { get; set; }

        [ForeignKey("ProductCategoryKey")]
        [InverseProperty("DimProductSubcategory")]
        public virtual DimProductCategory ProductCategoryKeyNavigation { get; set; }
        [InverseProperty("ProductSubcategoryKeyNavigation")]
        public virtual ICollection<DimProduct> DimProduct { get; set; }
    }
}
