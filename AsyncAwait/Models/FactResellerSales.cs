﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class FactResellerSales
    {
        public int ProductKey { get; set; }
        public int OrderDateKey { get; set; }
        public int DueDateKey { get; set; }
        public int ShipDateKey { get; set; }
        public int ResellerKey { get; set; }
        public int EmployeeKey { get; set; }
        public int PromotionKey { get; set; }
        public int CurrencyKey { get; set; }
        public int SalesTerritoryKey { get; set; }
        [StringLength(20)]
        public string SalesOrderNumber { get; set; }
        public byte SalesOrderLineNumber { get; set; }
        public byte? RevisionNumber { get; set; }
        public short? OrderQuantity { get; set; }
        [Column(TypeName = "money")]
        public decimal? UnitPrice { get; set; }
        [Column(TypeName = "money")]
        public decimal? ExtendedAmount { get; set; }
        public double? UnitPriceDiscountPct { get; set; }
        public double? DiscountAmount { get; set; }
        [Column(TypeName = "money")]
        public decimal? ProductStandardCost { get; set; }
        [Column(TypeName = "money")]
        public decimal? TotalProductCost { get; set; }
        [Column(TypeName = "money")]
        public decimal? SalesAmount { get; set; }
        [Column(TypeName = "money")]
        public decimal? TaxAmt { get; set; }
        [Column(TypeName = "money")]
        public decimal? Freight { get; set; }
        [StringLength(25)]
        public string CarrierTrackingNumber { get; set; }
        [Column("CustomerPONumber")]
        [StringLength(25)]
        public string CustomerPonumber { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? OrderDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DueDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ShipDate { get; set; }

        [ForeignKey("CurrencyKey")]
        [InverseProperty("FactResellerSales")]
        public virtual DimCurrency CurrencyKeyNavigation { get; set; }
        [ForeignKey("DueDateKey")]
        [InverseProperty("FactResellerSalesDueDateKeyNavigation")]
        public virtual DimDate DueDateKeyNavigation { get; set; }
        [ForeignKey("EmployeeKey")]
        [InverseProperty("FactResellerSales")]
        public virtual DimEmployee EmployeeKeyNavigation { get; set; }
        [ForeignKey("OrderDateKey")]
        [InverseProperty("FactResellerSalesOrderDateKeyNavigation")]
        public virtual DimDate OrderDateKeyNavigation { get; set; }
        [ForeignKey("ProductKey")]
        [InverseProperty("FactResellerSales")]
        public virtual DimProduct ProductKeyNavigation { get; set; }
        [ForeignKey("PromotionKey")]
        [InverseProperty("FactResellerSales")]
        public virtual DimPromotion PromotionKeyNavigation { get; set; }
        [ForeignKey("ResellerKey")]
        [InverseProperty("FactResellerSales")]
        public virtual DimReseller ResellerKeyNavigation { get; set; }
        [ForeignKey("SalesTerritoryKey")]
        [InverseProperty("FactResellerSales")]
        public virtual DimSalesTerritory SalesTerritoryKeyNavigation { get; set; }
        [ForeignKey("ShipDateKey")]
        [InverseProperty("FactResellerSalesShipDateKeyNavigation")]
        public virtual DimDate ShipDateKeyNavigation { get; set; }
    }
}
