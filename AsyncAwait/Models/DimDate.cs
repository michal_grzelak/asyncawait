﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimDate
    {
        public DimDate()
        {
            FactCallCenter = new HashSet<FactCallCenter>();
            FactCurrencyRate = new HashSet<FactCurrencyRate>();
            FactInternetSalesDueDateKeyNavigation = new HashSet<FactInternetSales>();
            FactInternetSalesOrderDateKeyNavigation = new HashSet<FactInternetSales>();
            FactInternetSalesShipDateKeyNavigation = new HashSet<FactInternetSales>();
            FactProductInventory = new HashSet<FactProductInventory>();
            FactResellerSalesDueDateKeyNavigation = new HashSet<FactResellerSales>();
            FactResellerSalesOrderDateKeyNavigation = new HashSet<FactResellerSales>();
            FactResellerSalesShipDateKeyNavigation = new HashSet<FactResellerSales>();
            FactSalesQuota = new HashSet<FactSalesQuota>();
            FactSurveyResponse = new HashSet<FactSurveyResponse>();
        }

        public int DateKey { get; set; }
        [Column(TypeName = "date")]
        public DateTime FullDateAlternateKey { get; set; }
        public byte DayNumberOfWeek { get; set; }
        [Required]
        [StringLength(10)]
        public string EnglishDayNameOfWeek { get; set; }
        [Required]
        [StringLength(10)]
        public string SpanishDayNameOfWeek { get; set; }
        [Required]
        [StringLength(10)]
        public string FrenchDayNameOfWeek { get; set; }
        public byte DayNumberOfMonth { get; set; }
        public short DayNumberOfYear { get; set; }
        public byte WeekNumberOfYear { get; set; }
        [Required]
        [StringLength(10)]
        public string EnglishMonthName { get; set; }
        [Required]
        [StringLength(10)]
        public string SpanishMonthName { get; set; }
        [Required]
        [StringLength(10)]
        public string FrenchMonthName { get; set; }
        public byte MonthNumberOfYear { get; set; }
        public byte CalendarQuarter { get; set; }
        public short CalendarYear { get; set; }
        public byte CalendarSemester { get; set; }
        public byte FiscalQuarter { get; set; }
        public short FiscalYear { get; set; }
        public byte FiscalSemester { get; set; }

        [InverseProperty("DateKeyNavigation")]
        public virtual ICollection<FactCallCenter> FactCallCenter { get; set; }
        [InverseProperty("DateKeyNavigation")]
        public virtual ICollection<FactCurrencyRate> FactCurrencyRate { get; set; }
        [InverseProperty("DueDateKeyNavigation")]
        public virtual ICollection<FactInternetSales> FactInternetSalesDueDateKeyNavigation { get; set; }
        [InverseProperty("OrderDateKeyNavigation")]
        public virtual ICollection<FactInternetSales> FactInternetSalesOrderDateKeyNavigation { get; set; }
        [InverseProperty("ShipDateKeyNavigation")]
        public virtual ICollection<FactInternetSales> FactInternetSalesShipDateKeyNavigation { get; set; }
        [InverseProperty("DateKeyNavigation")]
        public virtual ICollection<FactProductInventory> FactProductInventory { get; set; }
        [InverseProperty("DueDateKeyNavigation")]
        public virtual ICollection<FactResellerSales> FactResellerSalesDueDateKeyNavigation { get; set; }
        [InverseProperty("OrderDateKeyNavigation")]
        public virtual ICollection<FactResellerSales> FactResellerSalesOrderDateKeyNavigation { get; set; }
        [InverseProperty("ShipDateKeyNavigation")]
        public virtual ICollection<FactResellerSales> FactResellerSalesShipDateKeyNavigation { get; set; }
        [InverseProperty("DateKeyNavigation")]
        public virtual ICollection<FactSalesQuota> FactSalesQuota { get; set; }
        [InverseProperty("DateKeyNavigation")]
        public virtual ICollection<FactSurveyResponse> FactSurveyResponse { get; set; }
    }
}
