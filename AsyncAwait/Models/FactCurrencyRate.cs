﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class FactCurrencyRate
    {
        public int CurrencyKey { get; set; }
        public int DateKey { get; set; }
        public double AverageRate { get; set; }
        public double EndOfDayRate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Date { get; set; }

        [ForeignKey("CurrencyKey")]
        [InverseProperty("FactCurrencyRate")]
        public virtual DimCurrency CurrencyKeyNavigation { get; set; }
        [ForeignKey("DateKey")]
        [InverseProperty("FactCurrencyRate")]
        public virtual DimDate DateKeyNavigation { get; set; }
    }
}
