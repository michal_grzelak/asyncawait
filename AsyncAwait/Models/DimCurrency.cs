﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimCurrency
    {
        public DimCurrency()
        {
            DimOrganization = new HashSet<DimOrganization>();
            FactCurrencyRate = new HashSet<FactCurrencyRate>();
            FactInternetSales = new HashSet<FactInternetSales>();
            FactResellerSales = new HashSet<FactResellerSales>();
        }

        public int CurrencyKey { get; set; }
        [Required]
        [StringLength(3)]
        public string CurrencyAlternateKey { get; set; }
        [Required]
        [StringLength(50)]
        public string CurrencyName { get; set; }

        [InverseProperty("CurrencyKeyNavigation")]
        public virtual ICollection<DimOrganization> DimOrganization { get; set; }
        [InverseProperty("CurrencyKeyNavigation")]
        public virtual ICollection<FactCurrencyRate> FactCurrencyRate { get; set; }
        [InverseProperty("CurrencyKeyNavigation")]
        public virtual ICollection<FactInternetSales> FactInternetSales { get; set; }
        [InverseProperty("CurrencyKeyNavigation")]
        public virtual ICollection<FactResellerSales> FactResellerSales { get; set; }
    }
}
