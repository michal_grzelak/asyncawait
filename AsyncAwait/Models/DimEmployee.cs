﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimEmployee
    {
        public DimEmployee()
        {
            FactResellerSales = new HashSet<FactResellerSales>();
            FactSalesQuota = new HashSet<FactSalesQuota>();
            InverseParentEmployeeKeyNavigation = new HashSet<DimEmployee>();
        }

        public int EmployeeKey { get; set; }
        public int? ParentEmployeeKey { get; set; }
        [Column("EmployeeNationalIDAlternateKey")]
        [StringLength(15)]
        public string EmployeeNationalIdalternateKey { get; set; }
        [Column("ParentEmployeeNationalIDAlternateKey")]
        [StringLength(15)]
        public string ParentEmployeeNationalIdalternateKey { get; set; }
        public int? SalesTerritoryKey { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [StringLength(50)]
        public string MiddleName { get; set; }
        public bool NameStyle { get; set; }
        [StringLength(50)]
        public string Title { get; set; }
        [Column(TypeName = "date")]
        public DateTime? HireDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? BirthDate { get; set; }
        [Column("LoginID")]
        [StringLength(256)]
        public string LoginId { get; set; }
        [StringLength(50)]
        public string EmailAddress { get; set; }
        [StringLength(25)]
        public string Phone { get; set; }
        [StringLength(1)]
        public string MaritalStatus { get; set; }
        [StringLength(50)]
        public string EmergencyContactName { get; set; }
        [StringLength(25)]
        public string EmergencyContactPhone { get; set; }
        public bool? SalariedFlag { get; set; }
        [StringLength(1)]
        public string Gender { get; set; }
        public byte? PayFrequency { get; set; }
        [Column(TypeName = "money")]
        public decimal? BaseRate { get; set; }
        public short? VacationHours { get; set; }
        public short? SickLeaveHours { get; set; }
        public bool CurrentFlag { get; set; }
        public bool SalesPersonFlag { get; set; }
        [StringLength(50)]
        public string DepartmentName { get; set; }
        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [StringLength(50)]
        public string Status { get; set; }
        public byte[] EmployeePhoto { get; set; }

        [ForeignKey("ParentEmployeeKey")]
        [InverseProperty("InverseParentEmployeeKeyNavigation")]
        public virtual DimEmployee ParentEmployeeKeyNavigation { get; set; }
        [ForeignKey("SalesTerritoryKey")]
        [InverseProperty("DimEmployee")]
        public virtual DimSalesTerritory SalesTerritoryKeyNavigation { get; set; }
        [InverseProperty("EmployeeKeyNavigation")]
        public virtual ICollection<FactResellerSales> FactResellerSales { get; set; }
        [InverseProperty("EmployeeKeyNavigation")]
        public virtual ICollection<FactSalesQuota> FactSalesQuota { get; set; }
        [InverseProperty("ParentEmployeeKeyNavigation")]
        public virtual ICollection<DimEmployee> InverseParentEmployeeKeyNavigation { get; set; }
    }
}
