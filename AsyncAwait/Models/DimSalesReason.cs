﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimSalesReason
    {
        public DimSalesReason()
        {
            FactInternetSalesReason = new HashSet<FactInternetSalesReason>();
        }

        public int SalesReasonKey { get; set; }
        public int SalesReasonAlternateKey { get; set; }
        [Required]
        [StringLength(50)]
        public string SalesReasonName { get; set; }
        [Required]
        [StringLength(50)]
        public string SalesReasonReasonType { get; set; }

        [InverseProperty("SalesReasonKeyNavigation")]
        public virtual ICollection<FactInternetSalesReason> FactInternetSalesReason { get; set; }
    }
}
