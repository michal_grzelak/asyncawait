﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimScenario
    {
        [Key]
        public int ScenarioKey { get; set; }
        [StringLength(50)]
        public string ScenarioName { get; set; }
    }
}
