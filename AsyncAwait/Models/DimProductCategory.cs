﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimProductCategory
    {
        public DimProductCategory()
        {
            DimProductSubcategory = new HashSet<DimProductSubcategory>();
        }

        public int ProductCategoryKey { get; set; }
        public int? ProductCategoryAlternateKey { get; set; }
        [Required]
        [StringLength(50)]
        public string EnglishProductCategoryName { get; set; }
        [Required]
        [StringLength(50)]
        public string SpanishProductCategoryName { get; set; }
        [Required]
        [StringLength(50)]
        public string FrenchProductCategoryName { get; set; }

        [InverseProperty("ProductCategoryKeyNavigation")]
        public virtual ICollection<DimProductSubcategory> DimProductSubcategory { get; set; }
    }
}
