﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimDepartmentGroup
    {
        public DimDepartmentGroup()
        {
            InverseParentDepartmentGroupKeyNavigation = new HashSet<DimDepartmentGroup>();
        }

        [Key]
        public int DepartmentGroupKey { get; set; }
        public int? ParentDepartmentGroupKey { get; set; }
        [StringLength(50)]
        public string DepartmentGroupName { get; set; }

        [ForeignKey("ParentDepartmentGroupKey")]
        [InverseProperty("InverseParentDepartmentGroupKeyNavigation")]
        public virtual DimDepartmentGroup ParentDepartmentGroupKeyNavigation { get; set; }
        [InverseProperty("ParentDepartmentGroupKeyNavigation")]
        public virtual ICollection<DimDepartmentGroup> InverseParentDepartmentGroupKeyNavigation { get; set; }
    }
}
