﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimSalesTerritory
    {
        public DimSalesTerritory()
        {
            DimEmployee = new HashSet<DimEmployee>();
            DimGeography = new HashSet<DimGeography>();
            FactInternetSales = new HashSet<FactInternetSales>();
            FactResellerSales = new HashSet<FactResellerSales>();
        }

        public int SalesTerritoryKey { get; set; }
        public int? SalesTerritoryAlternateKey { get; set; }
        [Required]
        [StringLength(50)]
        public string SalesTerritoryRegion { get; set; }
        [Required]
        [StringLength(50)]
        public string SalesTerritoryCountry { get; set; }
        [StringLength(50)]
        public string SalesTerritoryGroup { get; set; }
        public byte[] SalesTerritoryImage { get; set; }

        [InverseProperty("SalesTerritoryKeyNavigation")]
        public virtual ICollection<DimEmployee> DimEmployee { get; set; }
        [InverseProperty("SalesTerritoryKeyNavigation")]
        public virtual ICollection<DimGeography> DimGeography { get; set; }
        [InverseProperty("SalesTerritoryKeyNavigation")]
        public virtual ICollection<FactInternetSales> FactInternetSales { get; set; }
        [InverseProperty("SalesTerritoryKeyNavigation")]
        public virtual ICollection<FactResellerSales> FactResellerSales { get; set; }
    }
}
