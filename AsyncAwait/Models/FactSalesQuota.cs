﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class FactSalesQuota
    {
        public int SalesQuotaKey { get; set; }
        public int EmployeeKey { get; set; }
        public int DateKey { get; set; }
        public short CalendarYear { get; set; }
        public byte CalendarQuarter { get; set; }
        [Column(TypeName = "money")]
        public decimal SalesAmountQuota { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Date { get; set; }

        [ForeignKey("DateKey")]
        [InverseProperty("FactSalesQuota")]
        public virtual DimDate DateKeyNavigation { get; set; }
        [ForeignKey("EmployeeKey")]
        [InverseProperty("FactSalesQuota")]
        public virtual DimEmployee EmployeeKeyNavigation { get; set; }
    }
}
