﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class FactSurveyResponse
    {
        public int SurveyResponseKey { get; set; }
        public int DateKey { get; set; }
        public int CustomerKey { get; set; }
        public int ProductCategoryKey { get; set; }
        [Required]
        [StringLength(50)]
        public string EnglishProductCategoryName { get; set; }
        public int ProductSubcategoryKey { get; set; }
        [Required]
        [StringLength(50)]
        public string EnglishProductSubcategoryName { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Date { get; set; }

        [ForeignKey("CustomerKey")]
        [InverseProperty("FactSurveyResponse")]
        public virtual DimCustomer CustomerKeyNavigation { get; set; }
        [ForeignKey("DateKey")]
        [InverseProperty("FactSurveyResponse")]
        public virtual DimDate DateKeyNavigation { get; set; }
    }
}
