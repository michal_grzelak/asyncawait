﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class FactInternetSalesReason
    {
        [StringLength(20)]
        public string SalesOrderNumber { get; set; }
        public byte SalesOrderLineNumber { get; set; }
        public int SalesReasonKey { get; set; }

        [ForeignKey("SalesOrderNumber,SalesOrderLineNumber")]
        [InverseProperty("FactInternetSalesReason")]
        public virtual FactInternetSales SalesOrder { get; set; }
        [ForeignKey("SalesReasonKey")]
        [InverseProperty("FactInternetSalesReason")]
        public virtual DimSalesReason SalesReasonKeyNavigation { get; set; }
    }
}
