﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimOrganization
    {
        public DimOrganization()
        {
            InverseParentOrganizationKeyNavigation = new HashSet<DimOrganization>();
        }

        [Key]
        public int OrganizationKey { get; set; }
        public int? ParentOrganizationKey { get; set; }
        [StringLength(16)]
        public string PercentageOfOwnership { get; set; }
        [StringLength(50)]
        public string OrganizationName { get; set; }
        public int? CurrencyKey { get; set; }

        [ForeignKey("CurrencyKey")]
        [InverseProperty("DimOrganization")]
        public virtual DimCurrency CurrencyKeyNavigation { get; set; }
        [ForeignKey("ParentOrganizationKey")]
        [InverseProperty("InverseParentOrganizationKeyNavigation")]
        public virtual DimOrganization ParentOrganizationKeyNavigation { get; set; }
        [InverseProperty("ParentOrganizationKeyNavigation")]
        public virtual ICollection<DimOrganization> InverseParentOrganizationKeyNavigation { get; set; }
    }
}
