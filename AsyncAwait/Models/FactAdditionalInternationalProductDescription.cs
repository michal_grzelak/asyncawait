﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class FactAdditionalInternationalProductDescription
    {
        public int ProductKey { get; set; }
        [StringLength(50)]
        public string CultureName { get; set; }
        [Required]
        public string ProductDescription { get; set; }
    }
}
