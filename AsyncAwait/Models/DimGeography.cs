﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsyncAwait.Models
{
    public partial class DimGeography
    {
        public DimGeography()
        {
            DimCustomer = new HashSet<DimCustomer>();
            DimReseller = new HashSet<DimReseller>();
        }

        public int GeographyKey { get; set; }
        [StringLength(30)]
        public string City { get; set; }
        [StringLength(3)]
        public string StateProvinceCode { get; set; }
        [StringLength(50)]
        public string StateProvinceName { get; set; }
        [StringLength(3)]
        public string CountryRegionCode { get; set; }
        [StringLength(50)]
        public string EnglishCountryRegionName { get; set; }
        [StringLength(50)]
        public string SpanishCountryRegionName { get; set; }
        [StringLength(50)]
        public string FrenchCountryRegionName { get; set; }
        [StringLength(15)]
        public string PostalCode { get; set; }
        public int? SalesTerritoryKey { get; set; }
        [StringLength(15)]
        public string IpAddressLocator { get; set; }

        [ForeignKey("SalesTerritoryKey")]
        [InverseProperty("DimGeography")]
        public virtual DimSalesTerritory SalesTerritoryKeyNavigation { get; set; }
        [InverseProperty("GeographyKeyNavigation")]
        public virtual ICollection<DimCustomer> DimCustomer { get; set; }
        [InverseProperty("GeographyKeyNavigation")]
        public virtual ICollection<DimReseller> DimReseller { get; set; }
    }
}
