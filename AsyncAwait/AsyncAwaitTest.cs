﻿using AsyncAwait.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Collections.Concurrent;

namespace AsyncAwait
{
    public static class AsyncAwaitTest
    {
        public static async Task ParallelAwaitDifferentObj()
        {
            Task<List<FactInternetSales>> task1 = GetListAsync<FactInternetSales>();
            Task<List<FactResellerSales>> task2 = GetListAsync<FactResellerSales>();

            List<FactInternetSales> internetSales = await task1;
            List<FactResellerSales> resellerSales = await task2;
        }

        public static async Task ParallelAwaitSameObj()
        {
            using (AWDbContext context = new AWDbContext())
            {
                Task<List<FactInternetSales>> task1 = context.FactInternetSales.ToListAsync();
                Task<List<FactResellerSales>> task2 = context.FactResellerSales.ToListAsync();

                List<FactInternetSales> internetSales = await task1;
                List<FactResellerSales> resellerSales = await task2;
            }
        }

        public static async Task SynchronousAwait()
        {
            using (AWDbContext context = new AWDbContext())
            {
                List<FactInternetSales> internetSales = await context.FactInternetSales.ToListAsync();
                List<FactResellerSales> resellerSales = await context.FactResellerSales.ToListAsync();
            }
        }

        public static async Task<List<T>> GetListAsync<T>() where T : class
        {
            using (AWDbContext context = new AWDbContext())
            {
                return await context.Set<T>().ToListAsync();
            }
        }

        public static async Task ParallelIterationWithoutYieldAwait(List<FactInternetSales> internetSales, List<FactResellerSales> resellerSales, int count = 1)
        {
            Task task1 = IterateOverListWithoutYield(internetSales, count);
            Task task2 = IterateOverListWithoutYield(resellerSales, count);

            await Task.WhenAll(task1, task2);
        }

        public static async Task ParallelIterationWithYieldAwait(List<FactInternetSales> internetSales, List<FactResellerSales> resellerSales, int count = 1)
        {
            Task task1 = IterateOverListWithYield(internetSales, count);
            Task task2 = IterateOverListWithYield(resellerSales, count);

            await Task.WhenAll(task1, task2);
        }

        public static async Task SynchronousIterationAwait(List<FactInternetSales> internetSales, List<FactResellerSales> resellerSales, int count = 1)
        {
            await IterateOverListWithoutYield(internetSales, count);
            await IterateOverListWithoutYield(resellerSales, count);
        }

        private static async Task IterateOverListWithYield<T>(List<T> list, int count = 1)
        {
            await Task.Yield();
            for (int i = 0; i < count; i++)
            {
                foreach (T item in list)
                {
                    var x = new Object();
                }
            }
        }

        private static async Task IterateOverListWithoutYield<T>(List<T> list, int count = 1)
        {
            for (int i = 0; i < count; i++)
            {
                foreach (T item in list)
                {
                    var x = new Object();
                }
            }
        }

        private async static Task<Object> CreateObject()
        {
            await Task.Yield();
            var x = new Object();
            return Task.FromResult<Object>(x);
        }

        public static async Task MixedTestAsynchronous(int fileCount = 3)
        {
            List<Task> tasks = new List<Task>();
            for (int i = 0; i < fileCount; i++)
            {
                tasks.Add(ProcessFile("dummy.txt", String.Format("{0}.txt", i)));
            }

            await Task.WhenAll(tasks);
            tasks.Clear();

            ConcurrentBag<Task<string>> stringTasks = new ConcurrentBag<Task<string>>();

            for (int i = 0; i < fileCount; i++)
            {
                stringTasks.Add(ReadFileAsync(String.Format("{0}.txt", i)));
            }

            await Task.WhenAll(stringTasks);
            StringBuilder sb = new StringBuilder();
            Parallel.ForEach(stringTasks, (task, state, index) =>
            {
                lock (sb)
                {
                    sb.AppendLine(task.Result);
                }
                File.Delete(String.Format("{0}.txt", index));
            });
        }

        public static async Task MixedTestSynchronous(int fileCount = 3)
        {
            for (int i = 0; i < fileCount; i++)
            {
                await ProcessFile("dummy.txt", String.Format("{0}.txt", i));
            }

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < fileCount; i++)
            {
                sb.AppendLine(await ReadFileAsync(String.Format("{0}.txt", i)));
                File.Delete(String.Format("{0}.txt", i));
            }
        }

        private static async Task ProcessFile(string readPath, string writePath)
        {
            string text = await ReadFileAsync(readPath);
            await WriteFileAsync(writePath, text);
        }

        private static async Task WriteFileAsync(string path, string text)
        {
            byte[] encodedText = Encoding.Unicode.GetBytes(text);

            using (FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize: 4096, useAsync: true))
            {
                await stream.WriteAsync(encodedText, 0, encodedText.Length);
            }
        }

        private static async Task<string> ReadFileAsync(string path)
        {
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: 4096, useAsync: true))
            {
                StringBuilder sb = new StringBuilder();

                byte[] buffer = new byte[0x1000];
                int numRead;
                while ((numRead = await stream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    string text = Encoding.Unicode.GetString(buffer, 0, numRead);
                    sb.Append(text.Take(text.Count() / 2));
                }

                return sb.ToString();
            }
        }
    }
}
