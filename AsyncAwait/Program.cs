﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using AsyncAwait.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace AsyncAwait
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddDbContext<AWDbContext>()
                .BuildServiceProvider();

            serviceProvider
                .GetService<ILoggerFactory>();

            var logger = serviceProvider.GetService<ILoggerFactory>()
                .CreateLogger<Program>();
            logger.LogDebug("Starting application");

            MainAsync(args).GetAwaiter().GetResult();

            Console.ReadKey();
        }

        public static async Task MainAsync(string[] args)
        {
            //await DBQueryTest();
            //await CollectionIterationTest();
            await MixedTest();
        }

        public static async Task DBQueryTest()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            await AsyncAwaitTest.ParallelAwaitSameObj();
            watch.Stop();
            Console.WriteLine(String.Format("Time of execution (in ms) for parallel DB Await with same obj usage: {0}", watch.ElapsedMilliseconds));

            watch.Reset();
            watch.Start();
            await AsyncAwaitTest.ParallelAwaitDifferentObj();
            watch.Stop();
            Console.WriteLine(String.Format("Time of execution (in ms) for parallel DB Await with different obj usage: {0}", watch.ElapsedMilliseconds));

            watch.Reset();
            watch.Start();
            await AsyncAwaitTest.SynchronousAwait();
            watch.Stop();
            Console.WriteLine(String.Format("Time of execution (in ms) for synchronous DB Await usage: {0}", watch.ElapsedMilliseconds));
        }

        public static async Task CollectionIterationTest()
        {
            List<FactInternetSales> internetSales = await AsyncAwaitTest.GetListAsync<FactInternetSales>();
            List<FactResellerSales> resellerSales = await AsyncAwaitTest.GetListAsync<FactResellerSales>();

            Stopwatch watch = new Stopwatch();
            watch.Start();
            await AsyncAwaitTest.ParallelIterationWithoutYieldAwait(internetSales, resellerSales, 3000);
            watch.Stop();
            Console.WriteLine(String.Format("Time of execution (in ms) for sample parallel Await Task on list without yield: {0}", watch.ElapsedMilliseconds));

            watch.Reset();
            watch.Start();
            await AsyncAwaitTest.ParallelIterationWithYieldAwait(internetSales, resellerSales, 3000);
            watch.Stop();
            Console.WriteLine(String.Format("Time of execution (in ms) for sample parallel Await Task on list with yield: {0}", watch.ElapsedMilliseconds));

            watch.Reset();
            watch.Start();
            await AsyncAwaitTest.SynchronousIterationAwait(internetSales, resellerSales, 3000);
            watch.Stop();
            Console.WriteLine(String.Format("Time of execution (in ms) for sample synchronous Await Task on list: {0}", watch.ElapsedMilliseconds));
        }

        public static async Task MixedTest()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            await AsyncAwaitTest.MixedTestAsynchronous(10);
            watch.Stop();
            Console.WriteLine(String.Format("Time of execution (in ms) for sample mixed asynchronous text: {0}", watch.ElapsedMilliseconds));

            watch.Reset();
            watch.Start();
            await AsyncAwaitTest.MixedTestSynchronous(10);
            watch.Stop();
            Console.WriteLine(String.Format("Time of execution (in ms) for sample mixed synchronous text: {0}", watch.ElapsedMilliseconds));
        }
    }
}
