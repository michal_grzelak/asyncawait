#Poprawne użycie Task'ów oraz async/await w C#:
Repozytorium: https://bitbucket.org/michal_grzelak/asyncawait/src/master/
Baza AdventureWorks (WAŻNE - wersja DW): https://docs.microsoft.com/en-us/sql/samples/adventureworks-install-configure?view=sql-server-2017

W root'cie projektu skrypt do stworzenia i zapełnienia bazy (niezależny od wersji SQL Server). W pliku skryptu jest instrukcja instalacji.

##Specyfikacja komputera:
8GB RAM, i7 4720HQ 3.4GHz, SSD ~500MB/S odczyt/zapis

##Wykonane testy to:
a) pobieranie wszystkich rekordów z dwóch tabel asynchronicznie "po kolei" oraz równocześnie
b) iteracja 3000 razy po każdej z dwóch list asynchronicznie "po kolei" oraz równocześnie

#1. Teoria:
* Powinniśmy unikać tworzenia nowych tasków przez `Task.Run(...)`. To tylko i wyłącznie powoduje, że obecny wątek czeka na wykonanie zadania i dodatkowo angażuje kolejny. Można użyć tylko i wyłącznie, gdy mamy punkt startowy programu (np. `Main`), który nie jest asynchroniczny i chcemy całą resztę kodu móc wykonać asynchronicznie (à la wrapper), lecz w innym przypadku nie ma to sensu i wtedy należy użyć funkcji async z await.

* Gdzie to możliwe powinniśmy używać async/await, ponieważ nawet jeśli posiadamy 1 wątek dla aplikacji, to zwiększamy jej skalowalność (przy zwiększeniu ilości wątków, bezpośrednio zwiększymy wydajność aplikacji).


#2. Testy:
* Wyniki testu są inne niż możnaby się tego spodziewać. W `Entity Framework` możemy wykonywać TYLKO 1 REQUEST NA RAZ DLA 1 INSTANCJI OBIEKTU `DBCONTEXT`.
"EF doesn't support processing multiple requests through the same DbContext object. If your second asynchronous request on the same DbContext instance starts before the first request finishes (and that's the whole point), you'll get an error message that your request is processing against an open DataReader." - źródło https://visualstudiomagazine.com/articles/2014/04/01/async-processing.aspx
###Wyniki:
Time of execution (in ms) for parallel DB Await with same obj usage: 22209
Time of execution (in ms) for parallel DB Await with different obj usage: 2985
Time of execution (in ms) for synchronous DB Await usage: 3487

Gdy użyjemy tego samego obiektu DbContext i spróbujemy wykonać requesty jednocześnie (zdefiniowanie Task'ów wcześniej, await'owanie później), następuje olbrzymie zapchanie. W tym przypadku request dotyczył różnych tabel dlatego nie został rzucony wyjątek.
Jednoczesny request możemy wykonać jeśli dla każdego z nich użyjemy osobno stworzonego obiektu klasy `DbContext` (UWAGA - nie powinniśmy tak robić, dlatego we wnioskach przedstawione jest rozwiązanie).

* W tym przypadku wyniki testu również sa inne niż mogłoby się wydawać. Dla przypomnienia ten test miał na celu iterację po dwóch listach zawierających ok 60 tys. rekordów (baza AdventureWorks) 3000 razy. 

###Wyniki:
Time of execution (in ms) for sample parallel Await Task on list without yield: 7721
Time of execution (in ms) for sample parallel Await Task on list with yield: 4657
Time of execution (in ms) for sample synchronous Await Task on list: 7881

Zdefiniowanie dwóch `Task`'ów przed, aby wykonać obie iteracje jednocześnie dla metod iterujących wcale nie poprawia wydajności w każdym wypadku. W pierwszym przypadku zostały wykonane synchronicznie. Dlaczego? Aby były asynchroniczne, muszą prawidłowo implementować asynchroniczność. W tym przypadku metoda iteruje po liście, więc nie posiada żadnej implementcji asynchroniczności. Możemy to zmienić używając `Task.Yield()` na początku funkcji, wymuszając asynchroniczność (wynik nr 2).

* Ostatni test pokazuje wczytywanie plików (bardzo duży plik 120MB), edycję danych plików, późniejszy zapis zedytowanych danych i usunięcie.
Time of execution (in ms) for sample mixed asynchronous text: 8365
Time of execution (in ms) for sample mixed synchronous text: 26551

Test jasno pokazuje, że warto używać metod asynchronicznych oraz przestrzeni Parallel. W tym przypadku różnica w wydajności jest trzykrotna.

#3. Wnioski:
* W większości przypadków powinniśmy używać jednej instancji `DbContext` na `WebRequest`. W `EF Core` używając `Dependency Injection` domyślnie funkcjonuje takie rozwiązanie. Jeśli chodzi o jednoczesne requesty, to niestety powinniśmy rozwiązać to dobrą optymalizacją zapytań oraz użyciem `IQueryable` w celu pobraniu wielu danych w jednym requeście.

* Samo dodanie async i zmiana typu zwrotnego na Task w funkcji nie zawsze jest wystarczająca. Jeśli funkcja asynchroniczna zawiera tylko kod synchroniczny to tak zostanie on wykonany. Można to zmienić używając w tej funkcji `Task.Yield()` wymuszając asynchroniczność. Trzeba jednak uważać, ponieważ zbyt dużo wywołań zapcha `ThreadPool` (np. w funkcji iterującej przeniesienie `Task.Yield()` do pętli powoduje użycie procesora 100%). Najlepiej gdzie się da, używać wbudowanych funkcji asynchronicznych i do iteracji metod w przestrzeni `Parallel` (pozwala też na wykonywanie kilku metod jednocześnie (`Parallel.Invoke(..)`)).